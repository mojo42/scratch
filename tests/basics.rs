extern crate scratch;

#[test]
fn part10() {
    scratch::run("
PROGRAM Part10;
VAR
   number     : INTEGER;
   a, b, c, x : INTEGER;
   y          : REAL;

BEGIN {Part10}
   BEGIN
      number := 2;
      a := number;
      b := 10 * a + 10 * number DIV 4;
      c := a - - b
   END;
   x := 11;
   y := 20 / 7 + 3.14;
END. {Part10}
")
}

#[test]
fn part11() {
    scratch::run("
PROGRAM Part11;
VAR
   number : INTEGER;
   a, b   : INTEGER;
   y      : REAL;

BEGIN {Part11}
   number := 2;
   a := number ;
   b := 10 * a + 10 * number DIV 4;
   y := 20 / 7 + 3.14
END.  {Part11}
")
}

#[test]
#[should_panic(expected = "variable 'a' is type 'integer")]
fn bad_type() {
    scratch::run("
PROGRAM Part11;
VAR
   a      : INTEGER;

BEGIN
   a := 3.5; {BOOM wrong type}
END.
	")
}

#[test]
#[should_panic(expected = "variable 'x' is type 'integer")]
fn bad_type_2() {
    scratch::run("
PROGRAM Part11;
VAR
   x      : INTEGER;
   y      : REAL;

BEGIN {Part11}
   y := 3.5;
   x := 2 + y; {BOOM bad type}
END.  {Part11}
")

}

#[test]
#[should_panic(expected = "symbol 'a' is not defined")]
fn var_does_not_exists() {
    scratch::run("
PROGRAM Part11;
VAR
   x      : INTEGER;
   y      : REAL;

BEGIN {Part11}
   y := 3.5;
   x := 2 + a; {BOOM a does not exist}
END.  {Part11}
	")
}

#[test]
fn basic_working_part12() {
    scratch::run("
PROGRAM Part12;
VAR
   a : INTEGER;

PROCEDURE P1;
VAR
   a : REAL;
   k : INTEGER;

   PROCEDURE P2;
   VAR
      a, z : INTEGER;
   BEGIN {P2}
      z := 777;
   END;  {P2}

BEGIN {P1}

END;  {P1}

BEGIN {Part12}
   a := 10;
END.  {Part12}
")
}

#[test]
#[should_panic(expected = "symbol 'y' is not defined")]
fn using_non_declared_variable() {
    scratch::run("
program Main;
	var x : integer;
begin
	x := y;
end.
")
}

#[test]
#[should_panic(expected = "variable 'x' has not been set")]
fn no_initialized_variables() {
    scratch::run("
program Main;
var x, y : integer;
begin
    x := x + y;
end.
")
}

#[test]
fn arithmetic_binop_int_to_real() {
    scratch::run("
program Main;
var x : real;
begin
    x := 1 / 2;
end.
")
}

#[test]
fn arithmetic_binop_real_to_int() {
    scratch::run("
program Main;
var x : integer;
begin
    x := 1.5 DIV 2;
end.
")
}

#[test]
fn procedure_with_params() {
    scratch::run("
program Main;
   var x, y: real;
   procedure Alpha(a : integer);
      var y : integer;
   begin
      x := a + x + y;
   end;
begin { Main }
end.  { Main }
")
}

// TODO: overloading with different signature is ok
/*#[test]
fn procedure_overloading_ok() {
    scratch::run("
PROGRAM test;
PROCEDURE P1;
BEGIN {P1}
END;  {P1}
PROCEDURE P1(a : integer);
BEGIN {P1}
END;  {P1}
BEGIN
END.
")
}*/

#[test]
#[should_panic(expected = "symbol 'P1' already defined")]
fn procedure_overloading_ko() {
    scratch::run("
PROGRAM test;
PROCEDURE P1;
BEGIN {P1}
END;  {P1}
PROCEDURE P1;
BEGIN {P1}
END;  {P1}
BEGIN
END.
")
}

#[test]
#[should_panic(expected = "symbol 'P1' already defined")]
fn procedure_double_define_nested() {
    scratch::run("
PROGRAM test;
PROCEDURE P1;
   PROCEDURE P1;
   BEGIN {P1}
   END;  {P1}
BEGIN {P1}
END;  {P1}
BEGIN {test}
END. {test}
")
}


