#[derive(PartialEq, Debug, Clone)]
pub enum Token {
    Plus,
    Minus,
    Mul,
    IntegerDiv,
    FloatDiv,
    IntegerConst(usize),
    RealConst(f64),
    LParen,
    RParen,
    Begin,
    End,
    Semi,
    Assign,
    Id(String),
    Dot,
    Eof,
    Program,
    Var,
    Colon,
    Comma,
    Integer,
    Real,
    Procedure,
}

// Lexer: basically take a stream of chars and transform them in Tokens
#[derive(Clone)]
pub struct Lexer {
    input: String,
    i: usize,
    c: Option<char>,
}

impl Lexer {
    pub fn new(input: &String) -> Lexer {
        Lexer {
            input: input.clone(),
            i: 0,
            c: input.chars().nth(0),
        }
    }

    pub fn next_token(&mut self) -> Token {
        loop {
            match self.c {
                None => return Token::Eof,
                Some(c) => match c {
                    '+' => {self.advance(); return Token::Plus},
                    '-' => {self.advance(); return Token::Minus},
                    '*' => {self.advance(); return Token::Mul},
                    '/' => {self.advance(); return Token::FloatDiv},
                    '(' => {self.advance(); return Token::LParen},
                    ')' => {self.advance(); return Token::RParen},
                    '.' => {self.advance(); return Token::Dot},
                    ';' => {self.advance(); return Token::Semi},
                    ',' => {self.advance(); return Token::Comma},
                    ':' => return self.read_colon_or_assign(),
                    '0'...'9' => return self.read_number(),
                    'A'...'z' => return self.read_id(),
                    '{' => {self.skip_comment(); continue},
                    _ => {self.advance(); continue},
                    //_ => panic!("unrecognized caracter '{}'", c),
                }
            }
        }
    }

    pub fn advance(&mut self) -> Option<char> {
        if self.i < self.input.len() - 1 {
            self.i += 1;
            self.c = self.input.chars().nth(self.i);
        } else {
            self.i = self.input.len();
            self.c = None
        }
        return self.c;
    }

    fn read_colon_or_assign(&mut self) -> Token {
        if let Some(c) = self.advance() {
            match c {
                '=' => {self.advance(); return Token::Assign},
                _ => {},
            };
        }
        Token::Colon
    }

    fn read_number(&mut self) -> Token {
        let mut i = String::new();
        i.push(self.c.unwrap());
        while let Some(c) = self.advance() {
            match c {
                '0'...'9' | '.' => i.push(c),
                _ => break,
            }
        }
        match i.parse::<usize>() {
            Ok(i) => return Token::IntegerConst(i),
            Err(_) => {},
        };
        match i.parse::<f64>() {
            Ok(f) => return Token::RealConst(f),
            Err(_) => {},
        };
        panic!("cannot parse number");
    }

    fn read_id(&mut self) -> Token {
        let mut id = String::new();
        id.push(self.c.unwrap());
        while let Some(c) = self.advance() {
            match c {
                'A'...'z' | '0'...'9' => id.push(c),
                _ => break,
            }
        }
        let lowered = id.to_lowercase();
        match lowered.as_ref() {
            "begin" => Token::Begin,
            "end" => Token::End,
            "div" => Token::IntegerDiv,
            "program" => Token::Program,
            "var" => Token::Var,
            "integer" => Token::Integer,
            "real" => Token::Real,
            "procedure" => Token::Procedure,
            _ => Token::Id(id),
        }
    }

    fn skip_comment(&mut self) {
        while let Some(c) = self.advance() {
            match c {
                '}' => {self.advance(); break},
                _ => {},
            };
        }
    }
}


