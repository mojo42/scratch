extern crate scratch;
use std::env;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    println!("Pascal Lang");
    let filename = env::args().nth(1).expect("provide file input");
    let mut f = File::open(filename).expect("file not found");
    let mut input = String::new();
    f.read_to_string(&mut input).expect("can't read file");
	scratch::run(input);	
}
