use std::cell::RefCell;
use std::collections::HashMap;
use ast;

// Lexical analyser: check if a correct grammar makes sens
pub struct Analyser {
    symbol_table: RefCell<SymbolTable>,
}

impl Analyser {
    pub fn new() -> Analyser {
        Analyser {
            symbol_table: SymbolTable::new(),
        }
    }

    pub fn run(&mut self, program: &ast::Program) {
        self.visit_program(program);
    }

    fn visit_program(&mut self, program: &ast::Program) {
        self.visit_block(&program.block);
    }

    fn visit_block(&mut self, block: &ast::Block) {
        for var in block.declarations.iter() {
            match var {
                &ast::Declaration::Variable(ref vd) => self.visit_var_declar(&vd),
                &ast::Declaration::Procedure(ref p) => self.visit_proc_declar(&p),
            };
        }
        self.visit_compound(&block.compound_statement);
    }

    fn visit_var_declar(&mut self, var: &ast::VariableDeclaration) {
        self.visit_type(&var.variable_type);
       let sym = Symbol::Variable{var_type: var.variable_type.name.clone()};
        self.symbol_table.borrow_mut().define(var.name.clone(), sym);
    }

    fn visit_proc_declar(&mut self, procedure: &ast::ProcedureDeclaration) {
        // TODO: need nested Symbols table
        //self.visit_block(&procedure.block);
        let sym = Symbol::Procedure;
        self.symbol_table.borrow_mut().define(procedure.name.clone(), sym);
    }

    fn visit_type(&mut self, var_type: &ast::Type) {
        match self.symbol_table.borrow().lookup(&var_type.name) {
            Symbol::BuiltinType => (),
            _ => panic!("{} is not a known type", var_type.name),
        };
    }

    fn visit_compound(&mut self, compound: &ast::Compound) {
        for s in compound.statements.iter() {
            self.visit_statement(s);
        }
    }

    fn visit_statement(&mut self, statement: &ast::Statement) {
        match statement {
            &ast::Statement::Assign(ref a) => self.visit_assign(&a),
            &ast::Statement::Compound(ref c) => self.visit_compound(&c),
            &ast::Statement::Empty => (),
        };
    }

    fn visit_assign(&mut self, assign: &ast::Assign) {
        let var_type = match self.symbol_table.borrow().lookup(&assign.variable.name) {
            Symbol::Variable{var_type} => (var_type),
            _ => panic!("cannot assign value to {}", assign.variable.name),
        };
        // Check that the value returned by visit_expr corresponds to the
        // variable's type.
        let expr_type = self.visit_expr(&assign.value);
        if var_type != expr_type {
            panic!("variable '{}' is type '{}', assigning type '{}'",
                   assign.variable.name, var_type, expr_type);
        }
    }

    fn visit_expr(&mut self, node: &Box<ast::Node>) -> String {
        match &**node {
            &ast::Node::Const(ref c) => self.visit_const(&c),
            &ast::Node::UnaryOp(ref u) => self.visit_expr(&u.value),
            &ast::Node::BinaryOp(ref b) => self.visit_bin_op(&b),
            &ast::Node::Variable(ref v) => self.visit_var(&v),
        }
    }

    fn visit_bin_op(&mut self, bo: &ast::BinaryOp) -> String {
        let real = "real".to_string();
        let int = "integer".to_string();
        match bo.op {
            ast::BinaryAction::FloatDiv => return real,
            ast::BinaryAction::IntegerDiv => return int,
            _ => {},
        };
        let left_type = self.visit_expr(&bo.left);
        let right_type = self.visit_expr(&bo.right);
        if left_type == real || right_type == real {
            return real;
        }
        left_type
    }

    fn visit_var(&mut self, variable: &ast::Variable) -> String {
        match self.symbol_table.borrow().lookup(&variable.name) {
            Symbol::Variable{var_type} => var_type,
            _ => panic!("{} not usable here", variable.name),
        }
    }

    fn visit_const(&mut self, c: &ast::Const) -> String{
        match c {
            &ast::Const::Integer(_) => "integer".to_string(),
            &ast::Const::Real(_) => "real".to_string(),
        }
    }
}

#[derive(Clone)]
pub enum Symbol {
    Variable{var_type: String},
    Procedure,
    BuiltinType,
}

pub struct SymbolTable {
    // Symbol name -> type
    symbols: HashMap<String, Symbol>,
	parent: Option<Box<RefCell<SymbolTable>>>,
}

impl SymbolTable {
    pub fn new() -> RefCell<SymbolTable> {
        let mut s = HashMap::new();
        s.insert("integer".to_string(), Symbol::BuiltinType);
        s.insert("real".to_string(), Symbol::BuiltinType);
        RefCell::new(SymbolTable {
            symbols: s,
			parent: None,
        })
    }

    pub fn define(&mut self, name: String, symbol_type: Symbol) {
        if self.symbols.get(&name).is_some() {
            panic!("symbol '{}' already defined", name)
        }
        self.symbols.insert(name, symbol_type);
    }

    pub fn lookup(&self, name: &String) -> Symbol {
        if let Some(s) = self.symbols.get(name) {
			return s.clone()
		} else if let Some(ref p) = self.parent {
			return p.borrow().lookup(&name)
		} else {
            panic!("symbol '{}' is not defined", name)
        }
    }
}
