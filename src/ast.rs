#[derive(Debug, Clone)]
pub struct Program {
    pub name: String,
    pub block: Block,
}

#[derive(Debug, Clone)]
pub struct Block {
    pub declarations: Vec<Declaration>,
    pub compound_statement: Compound,
}

#[derive(Debug, Clone)]
pub enum Declaration {
    Variable(VariableDeclaration),
    Procedure(ProcedureDeclaration),
}

#[derive(Debug, Clone)]
pub struct VariableDeclaration {
    pub name: String,
    pub variable_type: Type,
}

#[derive(Debug, Clone)]
pub struct ProcedureDeclaration {
    pub name: String,
    pub parameters: Vec<VariableDeclaration>,
    pub block: Box<Block>,
}

#[derive(Debug, Clone)]
pub struct Type {
    pub name: String,
}

#[derive(Debug, Clone)]
pub struct Variable {
    pub name: String,
}

#[derive(Debug, Clone)]
pub enum Const {
    Integer(usize),
    Real(f64),
}

#[derive(Debug, Clone)]
pub enum Statement {
    Compound(Compound),
    Assign(Assign),
    Empty,
}

#[derive(Debug, Clone)]
pub struct Compound {
    pub statements: Vec<Box<Statement>>,
}

#[derive(Debug, Clone)]
pub enum Node {
    Const(Const),
    Variable(Variable),
    UnaryOp(UnaryOp),
    BinaryOp(BinaryOp),
}

#[derive(Debug, Clone)]
pub struct Assign {
    pub variable: Variable,
    pub value: Box<Node>,
}

#[derive(Debug, Clone)]
pub enum UnaryAction {
    Plus,
    Minus,
}

#[derive(Debug, Clone)]
pub struct UnaryOp {
    pub op: UnaryAction,
    pub value: Box<Node>,
}

#[derive(Debug, Clone)]
pub enum BinaryAction {
    Plus,
    Minus,
    Mul,
    IntegerDiv,
    FloatDiv,
}

#[derive(Debug, Clone)]
pub struct BinaryOp {
    pub left: Box<Node>,
    pub op: BinaryAction,
    pub right: Box<Node>,
}
