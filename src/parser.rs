use ast;
use lexer as lex;

// Parser: basically takes a lex::Token stream and build an AST.
pub struct Parser {
    lexer: lex::Lexer,
    t: lex::Token,
}

impl Parser {
    pub fn new(lexer: &lex::Lexer) -> Parser {
        let mut l = (*lexer).clone();
        let t = l.next_token();
        Parser {
            lexer: l,
            t: t,
        }
    }

    pub fn parse(&mut self) -> ast::Program {
        self.program()
    }

    // program : PROGRAM variable SEMI block DOT
    fn program(&mut self) -> ast::Program {
        self.eat(lex::Token::Program);
        let id = self.variable();
        self.eat(lex::Token::Semi);
        let block = self.block();
        self.eat(lex::Token::Dot);
        ast::Program{name: id.name, block: block}
    }

    // block : declarations compound_statement
    fn block(&mut self) -> ast::Block {
        let d = self.declarations();
        let c = self.compound_statement();
        ast::Block {
            declarations: d,
            compound_statement: c,
        }
    }

    // declarations : (VAR (variable_declaration SEMI)+)*
    //              | (PROCEDURE ID (LPAREN formal_parameter_list RPAREN)? SEMI block SEMI)*
    //              | empty
    fn declarations(&mut self) -> Vec<ast::Declaration> {
        let mut decl = Vec::new();
        match self.t {
            lex::Token::Var => {
                self.next_token();
                decl.append(&mut self.variable_declaration());
                self.eat(lex::Token::Semi);
                loop {
                    match self.t {
                        lex::Token::Id(_) => {},
                        _ => break,
                    };
                    decl.append(&mut self.variable_declaration());
                    self.eat(lex::Token::Semi);
                }
            }
            _ => {},
        };
        loop {
            match self.t {
                lex::Token::Procedure => {
                    let mut parameters = Vec::new();
                    self.next_token();
                    let name = match self.t.clone() {
                        lex::Token::Id(name) => name,
                        _ => panic!("Expected identifier of procedure name"),
                    };
                    self.next_token();
                    match self.t {
                        lex::Token::LParen => {
                            self.next_token();
                            parameters = self.formal_parameter_list();
                            self.eat(lex::Token::RParen);
                        },
                        _ => {},
                    };
                    self.eat(lex::Token::Semi);
                    decl.push(ast::Declaration::Procedure(ast::ProcedureDeclaration {
                        name: name,
                        parameters: parameters,
                        block: Box::new(self.block()),
                    }));
                    self.eat(lex::Token::Semi);
                },
                _ => break,
            }
        }
        decl
    }

    // variable_declaration : ID (COMMA ID)* COLON type_spec
    fn variable_declaration(&mut self) -> Vec<ast::Declaration> {
        let mut name_list = Vec::<String>::new();
        match self.t.clone() {
            lex::Token::Id(name) => name_list.push(name),
            _ => panic!("Expected varible name"),
        };
        self.next_token();
        loop {
            match self.t {
                lex::Token::Comma => {
                    self.next_token();
                    match self.t.clone() {
                        lex::Token::Id(name) => name_list.push(name),
                        _ => panic!("Expected varible name after ','"),
                    }
                    self.next_token();
                },
                _ => break,
            };
        }
        self.eat(lex::Token::Colon);
        let var_type = self.type_spec().clone();
        let mut out = Vec::new();
        for name in name_list {
            out.push(ast::Declaration::Variable(
                ast::VariableDeclaration {
                    name: name,
                    variable_type: var_type.clone(),
                }));
        }
        out
    }

    // formal_parameter_list : formal_parameters
    //                       | formal_parameters SEMI formal_parameter_list
    fn formal_parameter_list(&mut self) -> Vec<ast::VariableDeclaration> {
        let mut list = Vec::new();
        loop {
            list.append(&mut self.formal_parameters());
            match self.t {
                lex::Token::Semi => {self.next_token(); continue},
                _ => break,
            }
        }
        list
    }

    // formal_parameters : ID (COMMA ID)* COLON type_spec
    fn formal_parameters(&mut self) -> Vec<ast::VariableDeclaration>{
        // Same grammar
        self.variable_declaration().iter()
			.map(|decl| match decl {
					&ast::Declaration::Variable(ref v) => v.clone(),
					&ast::Declaration::Procedure(_) => panic!("Cannot declare procedure declaration in procedure parameters"),
				})
			.collect()
    }

    // type_spec : INTEGER | REAL
    fn type_spec(&mut self) -> ast::Type {
        let out = match self.t {
            lex::Token::Integer => ast::Type{name: "integer".to_string()},
            lex::Token::Real => ast::Type{name: "real".to_string()},
            _ => panic!("Unexpected type"),
        };
        self.next_token();
        out
    }

    // compound_statement : BEGIN statement_list END
    fn compound_statement(&mut self) -> ast::Compound {
        self.eat(lex::Token::Begin);
        let r = ast::Compound {
            statements: self.statement_list(),
        };
        self.eat(lex::Token::End);
        r
    }

    // statement_list : statement
    //                | statement SEMI statement_list
    fn statement_list(&mut self) -> Vec<Box<ast::Statement>> {
        let mut l = Vec::new();
        l.push(self.statement());
        while self.t == lex::Token::Semi {
            self.eat(lex::Token::Semi);
            l.push(self.statement());
        }
        l
    }

    // empty :
    fn empty(&mut self) -> ast::Statement {
        ast::Statement::Empty
    }

    // statement : compound_statement
    //           | assignment_statement
    //           | empty
    fn statement(&mut self) -> Box<ast::Statement> {
        Box::new(match self.t {
            lex::Token::Begin => ast::Statement::Compound(self.compound_statement()),
            lex::Token::Id(_) => ast::Statement::Assign(self.assignement_statement()),
            _ => self.empty(),
        })
    }

    // assignment_statement : variable ASSIGN expr
    fn assignement_statement(&mut self) -> ast::Assign {
        let var = self.variable();
        self.eat(lex::Token::Assign);
        ast::Assign {
            variable: var,
            value: self.expr()
        }
    }

    // expr : term ((PLUS | MINUS) term)*
    fn expr(&mut self) -> Box<ast::Node> {
        let mut node = self.term();
        loop {
            let op = match self.t {
                lex::Token::Plus => ast::BinaryAction::Plus,
                lex::Token::Minus => ast::BinaryAction::Minus,
                _ => break,
            };
            self.next_token();
            let right = self.term();
            let bin_op = ast::BinaryOp {
                left: node,
                op: op,
                right: right,
            };
            node = Box::new(ast::Node::BinaryOp(bin_op));
        }
        return node;
    }

    // term : factor ((MUL | INTEGER_DIV | FLOAT_DIV) factor)*
    fn term(&mut self) -> Box<ast::Node> {
        let mut node = self.factor();
        loop {
            let op = match self.t {
                lex::Token::Mul => ast::BinaryAction::Mul,
                lex::Token::IntegerDiv => ast::BinaryAction::IntegerDiv,
                lex::Token::FloatDiv => ast::BinaryAction::FloatDiv,
                _ => break,
            };
            self.next_token();
            let right = self.factor();
            let bin_op = ast::BinaryOp {
                left: node,
                op: op,
                right: right,
            };
            node = Box::new(ast::Node::BinaryOp(bin_op));
        }
        return node;
    }

    // factor : PLUS factor
    //        | MINUS factor
    //        | INTEGER_CONST
    //        | REAL_CONST
    //        | LPAREN expr RPAREN
    //        | variable
    fn factor(&mut self) -> Box<ast::Node> {
       match self.t {
            lex::Token::Plus => {
                self.next_token();
                Box::new(ast::Node::UnaryOp(
                        ast::UnaryOp {
                    op: ast::UnaryAction::Plus,
                    value: self.factor(),
                }))
            },
            lex::Token::Minus => {
                self.next_token();
                Box::new(ast::Node::UnaryOp(
                        ast::UnaryOp {
                    op: ast::UnaryAction::Minus,
                    value: self.factor(),
                }))
            },
            lex::Token::IntegerConst(i) => {
                self.next_token();
                Box::new(ast::Node::Const(ast::Const::Integer(i)))
            },
            lex::Token::RealConst(f) => {
                self.next_token();
                Box::new(ast::Node::Const(ast::Const::Real(f)))
            },
            lex::Token::LParen => {
                self.eat(lex::Token::LParen);
                let n = self.expr();
                self.eat(lex::Token::RParen);
                n
            },
            lex::Token::Id(_) => Box::new(ast::Node::Variable(self.variable())),
            _ => panic!("unexpected"),
        }
    }

    // variable: ID
    fn variable(&mut self) -> ast::Variable {
        let name = match self.t.clone() {
            lex::Token::Id(s) => s,
            _ => panic!("not a variable"),
        };
        self.eat(lex::Token::Id(String::new()));
        ast::Variable{name: name}
    }

    fn next_token(&mut self) {
        self.t = self.lexer.next_token();
    }

    fn eat(&mut self, t: lex::Token) {
        // FIXME that's ugly
        match self.t {
            lex::Token::IntegerConst(_) => match t {
                lex::Token::IntegerConst(_) => (),
                _ => panic!("Expected {:?}, found {:?}", t, self.t),
            },
            lex::Token::Id(_) => match t {
                lex::Token::Id(_) => (),
                _ => panic!("Expected {:?}, found {:?}", t, self.t),
            },
            _ => if self.t != t {panic!("Expected {:?}, found {:?}", t, self.t)},
        };
        self.next_token();
    }
}
