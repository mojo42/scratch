use std::collections::HashMap;
use std::fmt;
use ast;

// Interpreter: depth traversal of an AST
pub struct Interpreter {
    globals: HashMap<String, f64>,
}

impl fmt::Display for Interpreter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.globals)
    }
}

impl Interpreter {
    pub fn new() -> Interpreter {
        Interpreter {
            globals: HashMap::new(),
        }
    }

    pub fn run(&mut self, program: &ast::Program) {
        self.visit_program(program);
    }

    // TODO: find a way to use a visitor pattern
    fn visit_program(&mut self, program: &ast::Program) {
        self.visit_block(&program.block);
    }

    fn visit_block(&mut self, block: &ast::Block) {
        for var in block.declarations.iter() {
            match var {
                &ast::Declaration::Variable(ref vd) => self.visit_var_declar(&vd),
                &ast::Declaration::Procedure(ref p) => self.visit_proc_declar(&p),
            };
        }
        self.visit_compound(&block.compound_statement);
    }

    fn visit_var_declar(&mut self, var: &ast::VariableDeclaration) {
        // TODO
        self.visit_type(&var.variable_type)
    }

    fn visit_proc_declar(&mut self, _procedure: &ast::ProcedureDeclaration) {
        // TODO
    }

    fn visit_type(&mut self, _type: &ast::Type) {
        // TODO
    }

    fn visit_compound(&mut self, compound: &ast::Compound) {
        for s in compound.statements.iter() {
            self.visit_statement(s);
        }
    }

    fn visit_statement(&mut self, statement: &ast::Statement) {
        match statement {
            &ast::Statement::Assign(ref a) => self.visit_assign(&a),
            &ast::Statement::Compound(ref c) => self.visit_compound(&c),
            &ast::Statement::Empty => (),
        };
    }

    fn visit_assign(&mut self, assign: &ast::Assign) {
        let value = self.visit_expr(&assign.value);
        self.globals.insert(assign.variable.name.clone(), value);
    }

    fn visit_expr(&mut self, node: &Box<ast::Node>) -> f64 {
        match &**node {
            &ast::Node::Const(ref c) => self.visit_const(&c),
            &ast::Node::UnaryOp(ref u) => match u.op {
                ast::UnaryAction::Plus => self.visit_expr(&u.value),
                ast::UnaryAction::Minus => - self.visit_expr(&u.value),
            },
            &ast::Node::BinaryOp(ref b) => match b.op {
                ast::BinaryAction::Plus => self.visit_expr(&b.left) + self.visit_expr(&b.right),
                ast::BinaryAction::Minus => self.visit_expr(&b.left) - self. visit_expr(&b.right),
                ast::BinaryAction::Mul => self.visit_expr(&b.left) * self.visit_expr(&b.right),
                ast::BinaryAction::IntegerDiv => (self.visit_expr(&b.left) as isize / self.visit_expr(&b.right) as isize) as f64,
                ast::BinaryAction::FloatDiv => self.visit_expr(&b.left) as f64 / self.visit_expr(&b.right) as f64,
            },
            &ast::Node::Variable(ref v) => self.visit_var(&v),
        }
    }

    fn visit_var(&mut self, variable: &ast::Variable) -> f64 {
        match self.globals.get(&variable.name) {
            Some(value) => *value,
            None => panic!("variable '{}' has not been set", variable.name),
        }
    }

    fn visit_const(&mut self, c: &ast::Const) -> f64 {
        match c {
            &ast::Const::Integer(i) => i as f64,
            &ast::Const::Real(f) => f,
        }
    }
}
