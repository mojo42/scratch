pub mod lexer;
pub mod ast;
pub mod parser;
pub mod analyser;
pub mod interpreter;

pub fn run<S: Into<String>>(sources: S) {
    let sources = sources.into();
    let mut lex = lexer::Lexer::new(&sources);
    let mut parser = parser::Parser::new(&mut lex);
    let p: ast::Program = parser.parse();
    let mut a = analyser::Analyser::new();
    a.run(&p);
    let mut int = interpreter::Interpreter::new();
    int.run(&p);
    println!("{:#?}", p);
    println!("{}", int);
}
